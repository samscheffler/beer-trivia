using System;
using System.Collections.Generic;
using System.Linq;
using BeerTrivia.Models;
using BeerTrivia.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BeerTrivia.Controllers
{
    [Route("api/[controller]")]
    public class TriviaQuestionController : Controller
    {
        private readonly ITriviaQuestionService _triviaQuestionService;

        public TriviaQuestionController(ITriviaQuestionService triviaQuestionService)
        {
            _triviaQuestionService = triviaQuestionService;
        }

        [HttpGet("GetActive")]
        public ActionResult<TriviaQuestion> GetActive()
        {
            var question = _triviaQuestionService.GetActive();

            return question;
        }

        [HttpGet("GetMostRecent")]
        public ActionResult<TriviaQuestion> GetMostRecent()
        {
            var question = _triviaQuestionService.GetMostRecent();

            return question;
        }

        [HttpGet("GetSubmittedAnswers")]
        public ActionResult<IEnumerable<TriviaQuestionAnswer>> GetSubmittedAnswers(int questionId)
        {
            return Ok(_triviaQuestionService.GetAnswers(questionId));
        }

        [HttpPost("Create")]
        public ActionResult<TriviaQuestion> Create([FromBody] TriviaQuestion question)
        {
            _triviaQuestionService.Create(question);

            return Ok(question);
        }

        [HttpPost("EndQuestion")]
        public void EndQuestion()
        {
            _triviaQuestionService.EndQuestion();
        }

        [HttpPost("SubmitAnswer")]
        public ActionResult<TriviaQuestionAnswer> SubmitAnswer([FromBody] TriviaQuestionAnswer answer)
        {
            _triviaQuestionService.CreateAnswer(answer);

            return Ok(answer);
        }

        [HttpGet("UserHasAnsweredQuestion")]
        public ActionResult<bool> UserHasAnsweredQuestion(int questionId, int userId)
        {
            return Ok(_triviaQuestionService.UserHasAnsweredQuestion(questionId, userId));
        }
    }
}