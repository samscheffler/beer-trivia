﻿using System.Collections.Generic;
using System.Linq;
using BeerTrivia.Models;
using BeerTrivia.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BeerTrivia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public List<User> Get()
        {
            return _userService.Get().OrderBy(u => u.Name).ToList();
        }
    }
}