﻿import Cookies from 'universal-cookie';

export default class CurrentUser {
    static get() {
        const cookies = new Cookies();

        return cookies.get('user');
    }

    static set(userId) {
        const cookies = new Cookies();

        cookies.set('user', userId);
    }
}