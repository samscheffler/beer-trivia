﻿import React, { Component } from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import HandleResponseError from 'services/ErrorHandler';
import CurrentUser from 'services/CurrentUser';

export default class UserList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            activeUser: null
        }

        this.onSelectUser = this.onSelectUser.bind(this);
    }

    componentDidMount() {
        fetch(`api/User`)
            .then(HandleResponseError)
            .then(response => response.json())
            .then(data => {
                this.setState({ users: data });
            })
            .catch(function (error) {
                console.log(error);
            });

        const userId = CurrentUser.get();

        this.setState({ activeUser: userId });
    }

    onSelectUser(id) {
        CurrentUser.set(id);

        this.setState({ activeUser: id });
    }

    render() {
        return (
            <ListGroup>
                {this.state.users.map((user, i) => {
                    const includeBsStyle = this.state.activeUser == user.id;
                    return (
                        <ListGroupItem
                            onClick={() => this.onSelectUser(user.id)}
                            key={user.id}
                            {...(includeBsStyle ? { bsStyle: 'success' } : {})}>
                            {user.name}
                        </ListGroupItem>
                    );
                })}
            </ListGroup>
        );
    }
}