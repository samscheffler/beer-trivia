﻿import React, { Component } from 'react';
import UserList from './UserList';
import handleResponseError from 'services/ErrorHandler';

export default class SelectUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: []
        }
    }

    componentDidMount() {
        fetch(`api/User`)
            .then(handleResponseError)
            .then(response => response.json())
            .then(data => {
                this.setState({ users: data });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <div>
                <h1>Who are you?</h1>
                <UserList></UserList>
            </div>
        );
    }
}