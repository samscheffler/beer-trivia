﻿import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import CurrentUser from 'services/CurrentUser';

export default class CreateQuestion extends Component {
    constructor(props, context) {
        super(props, context);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            question: '',
            answer: '',
            units: '',
            sourceUrl: '',
            creatorId: ''
        };
    }

    componentDidMount() {
        this.setState({
            creatorId: CurrentUser.get()
        });
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();

        fetch('/api/TriviaQuestion/Create',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    question: this.state.question,
                    units: this.state.units,
                    answer: this.state.answer,
                    sourceUrl: this.state.sourceUrl,
                    creatorId: this.state.creatorId
                })
            })
            .then(this.props.getActiveQuestion);
    }

    multiply(place) {
        var answer = this.state.answer;

        switch (place) {
            case 'thousand':
                if (answer < 1000) {
                    answer *= 1000;
                }
                break;
            case 'million':
                if (answer < 1000000) {
                    answer *= 1000000;
                }
                break;
            case 'billion':
                if (answer < 1000000000) {
                    answer *= 1000000000;
                }
                break;
            case 'trillion':
                if (answer < 1000000000000) {
                    answer *= 1000000000000;
                }
                break;
            default:
                break;
        }

        this.setState({ answer: answer });
    }

    render() {
        return (
            <div>
                <h1>Create your question!</h1>

                <hr />

                <form onSubmit={this.handleSubmit}>
                    <FormGroup
                        controlId="question"
                    >
                        <ControlLabel>Question</ControlLabel>
                        <FormControl
                            name="question"
                            required
                            type="text"
                            value={this.state.question}
                            placeholder="Enter a question"
                            onChange={this.handleChange}
                        />
                        <FormControl.Feedback />
                    </FormGroup>

                    <FormGroup
                        controlId="answer"
                    >
                        <ControlLabel>Answer</ControlLabel>
                        <FormControl
                            name="answer"
                            required
                            type="number"
                            step="any"
                            value={this.state.answer}
                            placeholder="Enter the answer"
                            onChange={this.handleChange}
                        />
                        <FormControl.Feedback />
                    </FormGroup>

                    <Button className="margin-right-20" onClick={() => this.multiply('thousand')}>
                        Thousand
                    </Button>
                    <Button className="margin-right-20" onClick={() => this.multiply('million')}>
                        Million
                    </Button>
                    <Button className="margin-right-20" onClick={() => this.multiply('billion')}>
                        Billion
                    </Button>
                    <Button className="margin-right-20" onClick={() => this.multiply('trillion')}>
                        Trillion
                    </Button>

                    <br />
                    <br />

                    <FormGroup
                        controlId="units"
                    >
                        <ControlLabel>Units</ControlLabel>
                        <FormControl
                            name="units"
                            required
                            type="text"
                            value={this.state.units}
                            placeholder="Enter the units (sq. ft., inches, etc.)"
                            onChange={this.handleChange}
                        />
                        <FormControl.Feedback />
                    </FormGroup>

                    <FormGroup
                        controlId="sourceUrl"
                    >
                        <ControlLabel>Source URL</ControlLabel>
                        <FormControl
                            name="sourceUrl"
                            required
                            type="text"
                            value={this.state.sourceUrl}
                            placeholder="Enter the source URL"
                            onChange={this.handleChange}
                        />
                        <FormControl.Feedback />
                    </FormGroup>

                    <Button type="submit" bsStyle="primary">Submit</Button>
                </form>
            </div>
        );
    }
}