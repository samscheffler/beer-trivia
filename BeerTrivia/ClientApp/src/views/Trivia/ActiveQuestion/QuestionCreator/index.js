﻿import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import handleResponseError from 'services/ErrorHandler';
import { Redirect } from 'react-router';

export default class QuestionCreatorView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            usersSubmitted: [],
            questionId: this.props.questionId,
            finished: false
        }

        this.checkForAnswers = this.checkForAnswers.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    checkForAnswers() {
        fetch(`/api/TriviaQuestion/GetSubmittedAnswers?questionId=${this.state.questionId}`)
            .then(handleResponseError)
            .then(response => response.json())
            .then(data => {
                this.setState({ usersSubmitted: data });
            })
            .catch(function (error) {
                console.log(error);
            });;
    }

    componentDidMount() {
        this.checkForAnswers();

        // Ping the server every second to get latest number of submitted answers
        this.interval = setInterval(() => this.checkForAnswers(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    handleSubmit(e) {
        e.preventDefault();

        fetch('/api/TriviaQuestion/EndQuestion',
            {
                method: 'POST'
            })
            .then(this.setState({ finished: true }));
    }

    render() {
        const submittedLanguage = this.state.usersSubmitted
            .length ==
            1
            ? 'person has'
            : 'people have';

        const contents = this.state.finished
            ? <Redirect to="/results" />
            : (
                <div>
                    <div style={{ 'margin': '20px 0' }}>
                        {this.state.usersSubmitted.length} {submittedLanguage} answered so far.
                    </div>

                    <form onSubmit={this.handleSubmit}>
                        <Button type="submit" bsStyle="primary">End Question</Button>
                    </form>
                </div>
            );

        return contents;
    }
}