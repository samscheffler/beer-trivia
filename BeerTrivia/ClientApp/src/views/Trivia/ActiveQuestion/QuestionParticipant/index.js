﻿import React, {Component} from 'react';
import {Form, FormGroup, FormControl, Button} from 'react-bootstrap';
import CurrentUser from 'services/CurrentUser';

export default class QuestionAnswererView extends Component {
  constructor (props) {
    super (props);

    this.state = {
      answer: 0,
      userId: -1,
      answered: false,
    };

    this.handleChange = this.handleChange.bind (this);
    this.handleSubmit = this.handleSubmit.bind (this);
    this.multiply = this.multiply.bind (this);
  }

  componentDidMount () {
    const userId = CurrentUser.get ();
    this.setState ({userId: userId});

    fetch (
      `/api/TriviaQuestion/UserHasAnsweredQuestion?questionId=${this.props.questionId}&userId=${userId}`
    )
      .then (response => response.json ())
      .then (data => {
        this.setState({answered: data})
      });
  }

  handleChange (e) {
    this.setState ({[e.target.name]: e.target.value});
  }

  handleSubmit (e) {
    e.preventDefault ();

    fetch ('/api/TriviaQuestion/SubmitAnswer', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify ({
        questionId: this.props.questionId,
        answer: this.state.answer,
        userId: this.state.userId,
      }),
    }).then (this.setState ({answered: true}));
  }

  multiply (place) {
    let answer = this.state.answer;
    switch (place) {
      case 'thousand':
        if (answer < 1000) {
          answer *= 1000;
        }
        break;
      case 'million':
        if (answer < 1000000) {
          answer *= 1000000;
        }
        break;
      case 'billion':
        if (answer < 1000000000) {
          answer *= 1000000000;
        }
        break;
      case 'trillion':
        if (answer < 1000000000000) {
          answer *= 1000000000000;
        }
        break;
      default:
        break;
    }

    this.setState ({answer: answer});
  }

  render () {
    if (this.state.answered) {
      return (
        <div>
          Thanks for your answer! The Beermeister will end the question when all answers have been submitted.
        </div>
      );
    }

    return (
      <div>
        <Form inline onSubmit={this.handleSubmit}>
          <FormGroup controlId="answer">
            <FormControl
              name="answer"
              required
              type="number"
              step="any"
              value={this.state.answer}
              placeholder="Enter your best guess!"
              onChange={this.handleChange}
              width={150}
            />

            <span style={{margin: '0 30px 0 10px'}}>{this.props.units}</span>
          </FormGroup>

          <Button
            className="margin-right-20"
            onClick={() => this.multiply ('thousand')}
          >
            Thousand
          </Button>
          <Button
            className="margin-right-20"
            onClick={() => this.multiply ('million')}
          >
            Million
          </Button>
          <Button
            className="margin-right-20"
            onClick={() => this.multiply ('billion')}
          >
            Billion
          </Button>
          <Button
            className="margin-right-20"
            onClick={() => this.multiply ('trillion')}
          >
            Trillion
          </Button>

          <br />
          <br />
          <FormGroup>
            <Button bsStyle="primary" type="submit">Submit Answer</Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}
