﻿import React, { Component } from 'react';
import CurrentUser from 'services/CurrentUser';
import QuestionCreator  from './QuestionCreator';
import QuestionParticipant  from './QuestionParticipant';

export default class ActiveQuestion extends Component {
    render() {
        return (
            <div>
                <h1>{this.props.question}</h1>

                <hr/>

                {CurrentUser.get() == this.props.creatorId ?
                    <QuestionCreator questionId={this.props.id}/> :
                    <QuestionParticipant userId={CurrentUser.get()} units={this.props.units} questionId={this.props.id}/>
                }
            </div>
        );
    }
}