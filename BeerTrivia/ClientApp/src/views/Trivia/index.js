import React, { Component } from 'react';
import { Redirect } from 'react-router';
import HandleResponseError from 'services/ErrorHandler';
import CreateQuestion from './CreateQuestion';
import ActiveQuestion from './ActiveQuestion';
import CurrentUser from 'services/CurrentUser';

export default class Trivia extends Component {
    displayName = Trivia.name;

    constructor(props) {
        super(props);

        this.state = {
            userId: null,
            loggedIn: true,
            activeQuestion: null
        }

        this.getActiveQuestion = this.getActiveQuestion.bind(this);
    }

    componentDidMount() {
        const userId = CurrentUser.get();

        if (userId >= 0) {
            this.setState({ loggedIn: true });
        } else {
            this.setState({ loggedIn: false });
        }

        this.getActiveQuestion();
    }

    getActiveQuestion() {
        fetch('api/TriviaQuestion/GetActive')
            .then(HandleResponseError)
            .then(response => response.json())
            .then(data => {
                this.setState({ activeQuestion: data});
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        if (this.state.loggedIn == false) {
            return <Redirect to="/selectuser"/>;
        }

        const contents = this.state.activeQuestion
            ? <ActiveQuestion {...this.state.activeQuestion}/>
            : <CreateQuestion getActiveQuestion={this.getActiveQuestion}/>;

        return (
            <div>
                {contents}
            </div>
        );
    }
}
