﻿import React, { Component } from 'react';
import Numeral from 'numeral';

export default class Result extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hidden: true
        };

        this.show = this.show.bind(this);
    }

    show() {
        this.setState({ hidden: false });
    }

    render() {
        const letterAns = Numeral(this.props.answer).format('0a');
        const commaAns = Numeral(this.props.answer).format('0,0');

        const contents = this.state.hidden
            ? <li className="clickable result" onClick={this.show} >Show result</li>
            : (
                <li className="result fadeIn" key={this.props.index}>
                    <strong>{this.props.user}</strong> with a guess of <strong>{letterAns}</strong> ({commaAns})
                </li>
            );

        return contents;
    }
}