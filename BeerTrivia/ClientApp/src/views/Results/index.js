﻿import React, { Component } from 'react';
import handleResponseError from 'services/ErrorHandler';
import './index.css';
import Result from './Result';
import Numeral from 'numeral';

export default class Results extends Component {
    constructor(props) {
        super(props);

        this.state = {
            question: {},
            sortedResults: [],
            isLoading: true
        }

        this.sortResults = this.sortResults.bind(this);
    }

    componentDidMount() {
        fetch('api/TriviaQuestion/GetMostRecent')
            .then(handleResponseError)
            .then(response => response.json())
            .then(data => {
                this.setState({ question: data, isLoading: false }, this.sortResults);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    sortResults() {
        const answers = [].concat(this.state.question.answers);

        answers.sort(function (a, b) {
            return a.placing - b.placing;
        });

        this.setState({ sortedResults: answers });
    }

    render() {
        const letterAns = Numeral(this.state.question.answer).format('0.00a');
        const commaAns = Numeral(this.state.question.answer).format('0,0');

        if(this.state.isLoading){
            return (<div></div>);
        }

        return (
            <div id="results">
                <h1>{this.state.question.question}</h1>

                <h2>
                    Answer: {letterAns} {this.state.question.units} ({commaAns})
                </h2>

                <h3>The results:</h3>

                <ol>
                    {this.state.sortedResults.map(function (answer, index) {
                        return (
                            <Result key={index} user={answer.user.name} answer={answer.answer} />
                        );
                    })}
                </ol>
            </div>
        );
    }
}