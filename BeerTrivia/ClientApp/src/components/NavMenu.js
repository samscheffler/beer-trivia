﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './NavMenu.css';

export class NavMenu extends Component {
  displayName = NavMenu.name

  render() {
    return (
      <Navbar inverse fixedTop fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>Beer Trivia</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/'} exact>
              <NavItem>
                <Glyphicon glyph="glass" /> Trivia!
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/selectuser'}>
              <NavItem>
                <Glyphicon glyph="user" /> Select User
              </NavItem>
                    </LinkContainer>
              <LinkContainer to={'/results'}>
                  <NavItem>
                      <Glyphicon glyph="star" /> Results
                  </NavItem>
              </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
