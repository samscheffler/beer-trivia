import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import  Trivia  from 'views/Trivia';
import SelectUser from 'views/SelectUser';
import Results from 'views/Results';

export default class App extends Component {
    displayName = App.name

    render() {
        return (
            <Layout>
                <Route exact path="/" component={Trivia} />
                <Route path="/selectuser" component={SelectUser} />
                <Route path="/results" component={Results} />
            </Layout>
        );
    }
}
