﻿using BeerTrivia.Models;
using Microsoft.EntityFrameworkCore;

namespace BeerTrivia
{
    public class BeerTriviaContext : DbContext
    {
        public BeerTriviaContext(DbContextOptions<BeerTriviaContext> options) : base(options) { }

        public DbSet<TriviaQuestion> TriviaQuestions { get; set; }
        public DbSet<TriviaQuestionAnswer> TriviaQuestionAnswers { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
