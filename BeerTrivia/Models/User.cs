﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
// ReSharper disable VirtualMemberCallInConstructor

namespace BeerTrivia.Models
{
    [Table("Users")]
    public class User
    {
        public User()
        {
            TriviaQuestionAnswers = new Collection<TriviaQuestionAnswer>();
            TriviaQuestions = new Collection<TriviaQuestion>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TriviaQuestionAnswer> TriviaQuestionAnswers { get; set; }
        public virtual ICollection<TriviaQuestion> TriviaQuestions { get; set; }
    }
}
