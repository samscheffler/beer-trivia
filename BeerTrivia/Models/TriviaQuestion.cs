﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
// ReSharper disable VirtualMemberCallInConstructor

namespace BeerTrivia.Models
{
    [Table("TriviaQuestions")]
    public class TriviaQuestion
    {
        public TriviaQuestion()
        {
            Answers = new Collection<TriviaQuestionAnswer>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Question { get; set; }
        public string Units { get; set; }
        public string SourceUrl { get; set; }
        public double Answer { get; set; }
        public int CreatorId { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateFinished { get; set; }

        public virtual User Creator { get; set; }
        public virtual ICollection<TriviaQuestionAnswer> Answers { get; set; }
    }
}
