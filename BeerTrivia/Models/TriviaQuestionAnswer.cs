﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeerTrivia.Models
{
    [Table("TriviaQuestionAnswers")]
    public class TriviaQuestionAnswer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public double Answer { get; set; }
        public int UserId { get; set; }
        public int QuestionId { get; set; }
        public DateTime DateAnswered { get; set; }
        public int? Placing { get; set; }

        public virtual TriviaQuestion Question { get; set; }
        public virtual User User { get; set; }
    }
}
