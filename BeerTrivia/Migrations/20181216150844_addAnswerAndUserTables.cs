﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BeerTrivia.Migrations
{
    public partial class addAnswerAndUserTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "TriviaQuestions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateFinished",
                table: "TriviaQuestions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "TriviaQuestions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TriviaQuestionAnswers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Answer = table.Column<double>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    DateAnswered = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TriviaQuestionAnswers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TriviaQuestionAnswers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TriviaQuestions_CreatorId",
                table: "TriviaQuestions",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_TriviaQuestionAnswers_UserId",
                table: "TriviaQuestionAnswers",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TriviaQuestions_Users_CreatorId",
                table: "TriviaQuestions",
                column: "CreatorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            var counter = 0;
            var names = new List<string>
            {
                "Dave",
                "David",
                "Perrin",
                "Preston",
                "Jason",
                "Kyle",
                "Ryan",
                "Sam",
                "Tom"
            };

            foreach (var name in names)
            {
                migrationBuilder.InsertData(
                    table: "Users",
                    columns: new[] { "Id", "Name" },
                    values: new object[] { counter, name });
                counter++;
            }
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TriviaQuestions_Users_CreatorId",
                table: "TriviaQuestions");

            migrationBuilder.DropTable(
                name: "TriviaQuestionAnswers");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropIndex(
                name: "IX_TriviaQuestions_CreatorId",
                table: "TriviaQuestions");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "TriviaQuestions");

            migrationBuilder.DropColumn(
                name: "DateFinished",
                table: "TriviaQuestions");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "TriviaQuestions");
        }
    }
}
