﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BeerTrivia.Migrations
{
    public partial class addPlacingColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Placing",
                table: "TriviaQuestionAnswers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Placing",
                table: "TriviaQuestionAnswers");
        }
    }
}
