﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BeerTrivia.Migrations
{
    public partial class addForeignKeyForAnswers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "QuestionId",
                table: "TriviaQuestionAnswers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TriviaQuestionAnswers_QuestionId",
                table: "TriviaQuestionAnswers",
                column: "QuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_TriviaQuestionAnswers_TriviaQuestions_QuestionId",
                table: "TriviaQuestionAnswers",
                column: "QuestionId",
                principalTable: "TriviaQuestions",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TriviaQuestionAnswers_TriviaQuestions_QuestionId",
                table: "TriviaQuestionAnswers");

            migrationBuilder.DropIndex(
                name: "IX_TriviaQuestionAnswers_QuestionId",
                table: "TriviaQuestionAnswers");

            migrationBuilder.DropColumn(
                name: "QuestionId",
                table: "TriviaQuestionAnswers");
        }
    }
}
