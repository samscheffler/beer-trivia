﻿using System;
using System.Collections.Generic;
using System.Linq;
using BeerTrivia.Models;
using BeerTrivia.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BeerTrivia.Services.Concretes
{
    public class TriviaQuestionService : ITriviaQuestionService
    {
        private readonly BeerTriviaContext _context;

        public TriviaQuestionService(BeerTriviaContext context)
        {
            _context = context;
        }

        public void Create(TriviaQuestion question)
        {
            question.DateCreated = DateTime.UtcNow;
            question.IsActive = true;

            _context.TriviaQuestions.Add(question);
            _context.SaveChanges();
        }

        public void EndQuestion()
        {
            var question = GetActive();

            question.DateFinished = DateTime.UtcNow;
            question.IsActive = false;

            var orderedAnswers = question.Answers.OrderBy(ans => Math.Abs(question.Answer - ans.Answer));
            var counter = 1;
            foreach (var answer in orderedAnswers)
            {
                answer.Placing = counter;
                counter++;
            }
            
            _context.SaveChanges();
        }

        public TriviaQuestion Get(int id)
        {
            return _context.TriviaQuestions.FirstOrDefault(tq => tq.Id == id);
        }

        public TriviaQuestion GetActive()
        {
            return _context
                .TriviaQuestions
                .Include(tq => tq.Answers)
                .SingleOrDefault(tq => tq.IsActive);
        }

        public void CreateAnswer(TriviaQuestionAnswer answer)
        {
            answer.DateAnswered = DateTime.UtcNow;
            _context.TriviaQuestionAnswers.Add(answer);
            _context.SaveChanges();
        }

        public IEnumerable<TriviaQuestionAnswer> GetAnswers(int questionId)
        {
            return _context
                .TriviaQuestionAnswers
                .Where(tqa => tqa.QuestionId == questionId)
                .Include(tqa => tqa.User);
        }

        public TriviaQuestion GetMostRecent()
        {
            return _context.TriviaQuestions
                .Where(tq => !tq.IsActive)
                .OrderByDescending(tq => tq.DateFinished)
                .Include(tq => tq.Answers)
                .ThenInclude(tqa => tqa.User)
                .FirstOrDefault();
        }

        public bool UserHasAnsweredQuestion(int questionId, int userId)
        {
            return _context
                .TriviaQuestionAnswers
                .Any(tqa => tqa.UserId == userId && tqa.QuestionId == questionId);
        }
    }
}
