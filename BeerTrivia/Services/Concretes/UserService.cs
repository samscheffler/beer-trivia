﻿using System.Collections.Generic;
using BeerTrivia.Models;
using BeerTrivia.Services.Interfaces;

namespace BeerTrivia.Services.Concretes
{
    public class UserService : IUserService
    {
        private readonly BeerTriviaContext _context;

        public UserService(BeerTriviaContext context)
        {
            _context = context;
        }

        public IEnumerable<User> Get()
        {
            return _context.Users;
        }
    }
}
