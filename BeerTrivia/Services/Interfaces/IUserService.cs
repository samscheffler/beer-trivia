﻿using System.Collections.Generic;
using BeerTrivia.Models;

namespace BeerTrivia.Services.Interfaces
{
    public interface IUserService
    {
        IEnumerable<User> Get();
    }
}
