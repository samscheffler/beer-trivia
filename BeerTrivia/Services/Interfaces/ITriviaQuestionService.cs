﻿using System.Collections.Generic;
using BeerTrivia.Models;

namespace BeerTrivia.Services.Interfaces
{
    public interface ITriviaQuestionService
    {
        void Create(TriviaQuestion question);
        TriviaQuestion Get(int id);
        TriviaQuestion GetActive();
        void EndQuestion();
        void CreateAnswer(TriviaQuestionAnswer answer);
        IEnumerable<TriviaQuestionAnswer> GetAnswers(int questionId);
        TriviaQuestion GetMostRecent();
        bool UserHasAnsweredQuestion(int questionId, int userId);
    }
}
